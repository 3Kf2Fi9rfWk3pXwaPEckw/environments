# Environments

This repository contains environment specifications for the Scientific Software Stack.

Legacy environment specifications are available:
* [SciDesktop environments](https://exxgitrepo:8443/projects/SSS/repos/scidesktop-environments/browse)
* [CrayHPC environments](https://exxgitrepo:8443/projects/SSS/repos/crayhpc-environments/browse)
